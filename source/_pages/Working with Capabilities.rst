=========================
Working with Capabilities
=========================

DesiredCapabilities Renamed
"""""""""""""""""""""""""""

In Selenium version 3.7.0, :code:`DesiredCapabilities` was renamed to :code:`MutableCapabilities` by the Selenium team. The capabilites API works the same as it always did, but as the framework utilizes Selenium version 3.7+, all users should at least be aware of the change.

----

Manually Creating Capabilities Object
"""""""""""""""""""""""""""""""""""""

To create a capabilities object, use the MutableCapabilities class. Below is an example of creating a capabilities object and passing it to a local driver.

.. code-block:: java

    // create capabilities object
    MutableCapabilities capabilities = new MutableCapabilities();

    // set some example capability keys and values
    capabilities.setCapability("browserName", "firefox");
    capabilities.setCapability("browserVersion", "57.0");

    // get a driver, pass a browser name and our capabilities object
    DriverFactory.getLocalDriver("firefox", capabilities)

----

Using Capabilities from Config File
"""""""""""""""""""""""""""""""""""

This section will explain how to use the configuration file's capabilities in the creation of a RemoteWebDriver.

1. Write or locate the line where your driver will be instantiated.

.. code-block:: java

    mWebDriver = DriverFactory.getRemoteDriver(...);


2. Get the capabilities object from the configuration instance and pass it to the driver creation call.

.. code-block:: java

    // get a remote driver from the factory, pass it our entire capabilities object, and
    // the remote connection URL from our capabilities config
    MutableCapabilities caps = Config.GetInstance().capabilities.getCapabilities();
    mWebDriver = DriverFactory.getRemoteDriver(caps, caps.capabilities.url);

=======

Next Steps
**********
:doc:`./Keyword System`
