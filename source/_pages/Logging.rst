=======
Logging
=======

.. todo:: link to maintenance documentation 

By default, the Log4J configuration file allows for the following: 

1) Folders are created for each day with the format: year/month/day 
2) Folders are created for each time the framework was executed within the day with the format: hour/minute/second 
3) Folders are created for each Thread, assuming that they set the ThreadContext variable "ThreadName" with their own individual log file inside. 
4) A larger, all-encompassing log file for the execution at the root of the time-stamped directory structure. 

Using the Logger
""""""""""""""""

The Log4J configuration included with the framework allows for the organization and separation of log-files into their threads context. 

By setting the "ThreadName" variable of the current ThreadContext, we ensure that a new folder and log will be created for the new "Thread1"

.. code-block:: java

	ThreadContext.put("ThreadName", "Thread1");

=======

Next Steps
**********
:doc:`./Reporting`
