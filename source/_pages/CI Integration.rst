==============
CI Integration
==============


The steps below are applicable to Bamboo but the steps are generic enough that they should apply to other continuous integration services. 

The lifecycle of the build process should look like the following: 

1. Checkout the source code of the project under test and perform a build.
2. Ensure the new project build is served to an accessible webserver for the testing framework to access
3. Checkout the source code of the testing framework 
4. Execute the testing framework


Preface
"""""""
The following instructions assume the following: 

1. A Project exists on the CI server already, and we wish to integrate into their buildchain
2. The framework tests are utilizing RemoteWebDrivers to connect to a dedicated testing machine (Selenium Grid)

----

Java Framework
""""""""""""""


Checkout
========
Create a new task in a new or existing plan that will checkout the source code of the Java framework using a specified repository URL and branch. 
Ensure that you are pulling a CLEAN build every time. 

Execution
=========
Create a new task in the plan used above and create a new Maven 3.X task with the following configuration:

1. Executable: The Maven executable on the build server. Ie:  :code:`C:\apache-maven-x.x.x\ `
2. Build JDK: :code:`JDK 1.8`
3. Goal: :code:`clean install exec:java -DskipTests=true`

Note: We skip the unit tests on the CI server currently because they are written to be used with a local WebDriver which is incapable of executing on the CI server and will produce a build failure.

JUnit Result Parsing (Bamboo)
=============================
After executing the framework, a :code:`TestResult.xml` file will be created in the root directory.

Some continuous integration services provide a parser for these results, such as the JUnit Result Parser or Maven 3.X runner in Bamboo.

To utilize this, modify the existing Maven 3.X task to attempt to locate the TestResult.xml file after execution using the following path:

.. code-block:: text: 

	**\TestResult.xml

Alternatively, create a new JUnit Parser task and modify the JUnit Test Result directory to be the above path.

----

C# Framework
""""""""""""


Checkout
========
Create a new task in a new or existing plan that will checkout the source code of the C# framework using a specified repository URL and branch. 
Ensure that you are pulling a CLEAN build every time. 

Package Manager Updates
=======================
1. Download and install the NuGet executable to the build server (NuGet standalone executable is available at https://www.nuget.org/downloads)
2. Create a new script task with the following line of code

.. code-block:: text

	"C:\YOUR_NUGET_DIRECTORY\nuget.exe" restore "${bamboo.build.working.directory}\YOUR_FRAMEWORK_DIRECTORY\csharp-generic-framework.sln"

- Replace "YOUR_NUGET_DIRECTORY" with the path to the folder your NuGet is installed to

- Replace "YOUR_FRAMEWORK_DIRECTORY" with the path to your cloned csharp generic framework folder

Build
=====
    **NOTE** The MSBuild executable on the build server must be version 14.0 or higher. MSBuild is bundled with the .NET framework, which can be downloaded at https://www.microsoft.com/net/download/.

Execute the following command either through a build task supporting MSBuild, or by calling MSBuild.exe directly via command prompt on the Bamboo server machine

.. code-block:: text

	MSBuild.exe MySolution.sln /p:Configuration="ConfigName";Platform="PlatformName"


e.g: 

.. code-block:: text

	MSBuild.exe "YOUR_FRAMEWORK_DIRECTORY\csharp-generic-framework.sln" /p:Configuration="Debug";Platform="All CPU"

- Replace "YOUR_FRAMEWORK_DIRECTORY" with the path to your cloned csharp generic framework folder

**NOTE** If the /p flag gives errors, omitting it should be fine; MSBuild should use Debug and All CPU as its defaults.

Alternatively, most CI tools will allow this through a plug-in and should assist with putting the arguments in their necessary location. 

Execution
=========

In Bamboo, Create a new task of type "Script" and call the executable that was built in the previous step. It will be in a hierarchy of directories related to the build configuration (:code:`project root\project name\bin\Debug\` by default)

Provide any of the optional command-line arguments here. 

e.g task script body:

.. code-block:: text

    "YOUR_FRAMEWORK_DIRECTORY\csharp-generic-framework\csharp-generic-framework\bin\Debug\csharp-generic-framework.exe" --config "PATH_TO_CONFIG_TOML_FILE"

- Replace "YOUR_FRAMEWORK_DIRECTORY" with the path to your cloned csharp generic framework folder

- Replace "PATH_TO_CONFIG_TOML_FILE" with the path to the framework :code:`Config.toml` you wish to use with this framework execution


The job should now be ready to execute.

NUnit Result Parsing (Bamboo)
=============================
After executing the framework, a :code:`TestResult.xml` file will be created in the build directory.
Some continuous integration services provide a parser for these results, such as the NUnit Result Parser in Bamboo.

To utilize this, create a new NUnit Parser task and modify the NUnit Test Result directory to be the following: 

.. code-block:: text 

	\**\TestResult.xml


=======

Next Steps
**********
:doc:`./Logging`
