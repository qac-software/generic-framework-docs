=========
Reporting
=========

----

Java Framework
""""""""""""""

By default in the Java generic framework, the JUnitCore will have a :code:`AllureJUnit4` appended as a JUnit run listener which will
produce test results in a folder post-execution with the name :code:`allure-results` in the root directory of the project. 

Though no intervention is required past this, the Allure adapter also offers some helper annotations that can be used with your tests
to better describe the tests for the report. 

For example, the JUnit :code:`@Test` method below has been annotated with additional Allure annotations.

Please see section :code:`5.1.2. Features` in the `Allure documentation <https://docs.qameta.io/allure/latest/#_junit_4>`_ for more annotation information.

.. code-block:: java

    @Test
    @Tag("JUnit")
    @Issue("TestIssue#1")
    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description")
    public void myTestMethod(){
        ...
    }

The result files are saved in the root directory of the framework. Ie: :code:`...\java-generic-framework\allure-results`

----

C# Framework
""""""""""""

By default in the C# generic framework, Allure reporting is enabled. However, unlike the Java framework we must enable each of our tests to be gathered by the reporter. 
This is done by annotating the Test Fixtures and Tests with additional Allure annotations provided by the :code:`NUnit.Allure.Attributes` package. 

Please see the `Allure-NUnit git repository <https://github.com/unickq/allure-nunit>`_ for more annotation information.

.. code-block:: csharp

    [TestFixture]
    [AllureFixture]
    class TestCase
    {
        [Test]
        [AllureTest("Test Name")]
        [AllureTag("NUnit","Debug")]
        [AllureIssue("TestIssue#1")]
        [AllureSeverity(AllureSeverity.Normal)]
        public void MyTestMethod()
        {
            ...
        }

The result files are saved in the build directory of the framework. Ie: :code:`...\csharp-generic-framework\csharp-generic-framework\bin\Debug\allure-results`

----

Generating Report
"""""""""""""""""

Allure will only produce an :code:`allure-results` folder, and will still need to be generated through Allure's command-line tools.
The batch file :code:`GenerateReport.bat` has been created to assist with this. 

For Java, this batch file assumes that the results are stored in: :code:`...\java-generic-framework\allure-results`

For C#, this batch file assumes that the results are stored in: :code:`...\csharp-generic-framework\csharp-generic-framework\bin\Debug\allure-results`

----

Serving Report
""""""""""""""

Allure reports must be served from a web-server. Thankfully, Allure comes with one built in and the helper batch file :code:`ServeReport.bat` was created to assist with this. 

For Java, this batch file assumes that the generated report is stored in: :code:`...\java-generic-framework\allure-report`

For C#, this batch file assumes that the generated report is stored in: :code:`...\csharp-generic-framework\allure-report`

=====

Next Steps
**********
:doc:`./Remote Services`

