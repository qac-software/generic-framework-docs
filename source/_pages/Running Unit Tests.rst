==========
Unit Tests
==========

Unit tests are useful for ensuring our Page classes and test scripts are behaving correctly.

Unit tests are located in the :code:`src/test/java/qac/` directory, and are named after the functionality they are intended to test. Basic unit tests for the core of the framework are included and are a good reference point for adding your own.

Creating a Unit Test
""""""""""""""""""""

Java
----

1. Create a new class in the :code:`src/test/java/qac/` directory and give it a name that represents what part of the project it will be testing. No class constructor method is needed

2. Add your test methods to the class, and give them the JUnit *Test* annotation as shown below

.. code-block:: java

    // make sure to add the Junit @Test annotation here:
    @Test
    public void testMyMethod() throws Exception {
        // test method contents...
    }

    // ... more test methods ...

C#
--

1. Create a new folder inside the :code:`csharp-generic-framework/src/UnitTests/` folder, and name it after what your unit test is testing.

2. Create a new C# class inside the folder you've just created, and name it the same as the folder. No class constructor method is needed.

3. Add the NUnit *TestFixture* annotation above your class name as shown in the code example below.

4. Add your test methods, each with the NUnit *Test* annotation as shown in the code example below.

.. code-block:: C#

    namespace csharp_generic_framework.src.UnitTests.TestMyFeature
    {
        [TestFixture]
        class TestMyFeature
        {
            [Test]
            public void TestMyMethod()
            {
                // test method contents...
            }

            // ... more test methods ...
        }
    }


Running Unit Tests
""""""""""""""""""

Run a Single Unit Test
----------------------

**Eclipse**

Right click the test in the Package Explorer and select "Run As > JUnit Test"

**IntelliJ IDEA**

Right click the class in the project explorer and click the Run option.

**Visual Studio (C# Framework)**

Open the Program.cs file and find the "Unit Tests" comment block inside the Main method. Un-comment the block which says *UNCOMMENT BELOW TO RUN INDIVIDUAL UNIT TESTS*, and make sure the list contains only the unit tests you wish to run. An example of the result is shown below.

.. code-block:: C#
 
    /* 
     * UNCOMMENT BELOW TO RUN INDIVIDUAL UNIT TESTS
     */
    new AutoRun().Execute(BuildNUnitArguments(new string[] {
    	"csharp_generic_framework.src.UnitTests.TestConfig",
    	"csharp_generic_framework.src.UnitTests.TestDriverFactory",
    	"csharp_generic_framework.src.UnitTests.TestKeywords",
    	"csharp_generic_framework.src.UnitTests.TestPage",
    }));

NOTE: You will likely also want to comment out the test suite AutoRun block located up several lines in the Main method, or else your test cases will run before the Unit Tests do.


Run All Unit Tests
------------------

Running the Maven goal :code:`test` will run all the tests present in :code:`src/test/java/qac/` and all of its sub-directories.

**Eclipse**

Right click on the pom.xml in the Package Explorer and select Run As > Maven test

**IntelliJ IDEA**

Open the "Maven Projects" pane and double click "test".

**Visual Studio (C# Framework)**

Open the Program.cs file and find the "Unit Tests" comment block inside the Main method. Un-comment the block which says *UNCOMMENT BELOW TO RUN ALL UNIT TESTS IN ONE EXECUTION*. An example of the result is shown below.

.. code-block:: C#
 
    /*
     * UNCOMMENT BELOW TO RUN ALL UNIT TESTS IN ONE EXECUTION
     */
    new AutoRun().Execute(BuildNUnitArguments(new string[] {
    	"csharp_generic_framework.src.UnitTests",
    }));

NOTE: You will likely also want to comment out the test suite AutoRun block located up several lines in the Main method, or else your test cases will run before the Unit Tests do.



