=============
Running Tests
=============

Creating the Driver
"""""""""""""""""""
As shown in the previous page :doc:`./Create Tests`, a test script contains a setup method that creates a driver instance for the test to use. In that example, the QAC DriverFactory utility class was used; This is a class that provides driver creation methods for the most common browsers.


.. code-block:: java

    @Before
    public void setup() throws Exception {
        MutableCapabilities myCapabilities = new MutableCapabilities();

        // tell the driver to use a non-standard browser executable
        myCapabilities.setCapability("binary", "/usr/lib/chromium-browser/chromium-browser");

        driver = DriverFactory.getLocalDriver(DriverFactory.Browser.CHROME, myCapabilities);
    }

The DriverFactory is not required to create a test; a driver can still be created the standard way.

.. code-block:: java

    @Before
    public void setup() throws Exception {
        // create a ChromeDriver
        driver = new ChromeDriver();
    }

**NOTE** The code samples above are Java. The C# method and annotation syntax will look slightly different, but the driver creation process will be the same.

----

Running tests in sequence
"""""""""""""""""""""""""

Java
****

Once a test class has been created (see previous page :doc:`./Create Tests`), the class has to be added to the test runner.

1. Create a new class that will represent our test suite with an explicit name - it is probably best to include the word `Suite` in the name - and add the annotation :code:`@SuiteClasses()` containing an array of test .class files. 

.. code-block:: java

    @SuiteClasses({
        ExampleTest.class,
    })

    public class ExampleSuite {
        /*
         * This area is intentionally left blank.
         * The annotation will take care of all of the heavy lifting.
         */
    }


2. Find the location in :code:`src/main/java/com/qaconsultants/App` which assigns a value to the :code:`Class<?>[] _suites` variable and append the newly created suite to the array. 

.. code-block:: java

    Class<?>[] _suites = {
        ExampleSuite.class,
    };

3. Run the framework.

   - **Eclipse** run the :code:`exec:java` maven goal
   - **IntelliJ IDEA** right click "App" in the project sidebar and select *Run 'App.main()'*
   - See the Getting Started page for more detailed instructions.


C#
**

1. Open :code:`csharp-generic-framework/src/Program.cs` and locate the AutoRun Execute method call inside Main. Add a string representation of your test case class to the array argument. An example is shown below.

.. code-block:: C#
   :emphasize-lines: 2

    new AutoRun().Execute(BuildNUnitArguments(new string[] {
        "csharp_generic_framework.Tests.ExampleSuite"
    }));

----

Running tests in parallel (test suite)
""""""""""""""""""""""""""""""""""""""

Java
****

1. Following the instructions above for running the tests in sequence, you should have a new class that looks something like the following: 

.. code-block:: java

    @SuiteClasses({
        ExampleTest.class,
    })
    public class ExampleSuite {}

2. Append the following annotation to the suite, either above or below the :code:`@SuiteClasses()` annotation

.. code-block:: java

    @RunWith(ParallelSuite.class) 


.. code-block:: java
    :emphasize-lines: 1

    @RunWith(ParallelSuite.class) 
    @SuiteClasses({
        ExampleTest.class,  //Runs on thread 1
        ExampleTest2.class  //Runs on thread 2
    })
    public class ExampleSuite {}

3. Run the framework.

   - **Eclipse** run the :code:`exec:java` maven goal
   - **IntelliJ IDEA** right click "App" in the project sidebar and select *Run 'App.main()'*
   - **Visual Studio** (C#) Press the green arrow *Start* button at the top of the window
   - See the Getting Started page for more detailed instructions.


C#
**

**NOTE**: The C# version will run each test case inside a suite in parallel, but run each suite listed in Main() in sequence.

1. Create a new folder inside the :code:`csharp-generic-framework/src/Tests/Suites/` folder, and name it after your test suite. It is probably best to include the word `Suite` in the name.

2. Inside this newly created folder, create a new Class called :code:`SuiteSetup`.

3. Add the square bracket annotations shown below to the lines above the class definition line

.. code-block:: C#

    namespace csharp_generic_framework.Tests.Suites.ExampleSuite
    {
        [SetUpFixture]
        [Parallelizable(ParallelScope.Fixtures)]
        public class Setup
        {}
    }

4. Make sure *all* of the individual test scripts that the suite should run are in the same folder; any tests that the suite named ExampleSuite should run need to be placed in the :code:`Suites/ExampleSuite` directory. The C# framework suite classes work by running any adjacent classes that exist in their current folder.

5. Open :code:`csharp-generic-framework/src/Program.cs` and locate the AutoRun Execute method call inside Main. Add a string representation of your suite class path to the array argument. An example is shown below.

.. code-block:: C#
   :emphasize-lines: 2

    new AutoRun().Execute(BuildNUnitArguments(new string[] {
        "csharp_generic_framework.Tests.Suites.ExampleSuite"
    }));

3. Run the framework.

   - **Visual Studio** (C#) Press the green arrow *Start* button at the top of the window
   - See the Getting Started page for more detailed instructions.

=======

Next Steps
**********
:doc:`./Configuration`
