==================
Command Line Usage
==================
To interface with product management toolchains and processes such as continuous integration, the framework uses a standard command-line interface alongside its configuration file for providing the framework with the necessary run-time information. 

----

Java Framework
""""""""""""""
Requirement(s): 
`Maven <https://maven.apache.org>`_

To execute the framework from the command-line, we utilize Maven and the `Apache exec plugin <http://www.mojohaus.org/exec-maven-plugin/index.html>`_ with the following configuration: 

.. code-block:: xml

	<plugin>
		<groupId>org.codehaus.mojo</groupId>
		<artifactId>exec-maven-plugin</artifactId>		
		<version>1.2.1</version>
		<configuration>
			<mainClass>com.qaconsultants.App</mainClass>
		</configuration>
	</plugin>

When performing any of the following commands, ensure that the working directory of your terminal is set to the framework directory.


Note: If the :code:`mvn` command can not be found, see the "Maven in PATH" section `here <https://maven.apache.org/guides/getting-started/windows-prerequisites.html>`_


Compilation
===========
To compile the framework using Maven, execute the following command:

.. code-block:: text

	mvn compile 


Packaging
=========
To package the framework into an executable archive (.jar) with its dependencies, execute the following command

.. code-block:: text

	mvn package assembly:single 


Execution
=========
To begin the framework, execute the following command: 

.. code-block:: text

	mvn exec:java

Alternatively, the framework may be set-up to take in arguments at run-time. To take advantage of this, we can provide arguments using a slight modification to the command above: 

.. code-block:: text
	
	mvn exec:java -Dexec.args="-arg1 -arg2 -arg3" 

----

C# Framework
""""""""""""
Requirement(s): 
`MSBuild <https://www.microsoft.com/en-us/download/details.aspx?id=48159>`_/`Visual Studio <https://www.visualstudio.com>`_

Compilation
===========
Normally, you will use Visual Studio to compile the project. However, if you would like an alternative method of doing so, you can use MSBuild (version 14.0 or higher) with the following command:

.. code-block:: text 

	MSBuild.exe csharp-generic-framework.sln /p:Configuration="ConfigName";Platform="PlatformName"

e.g.

.. code-block:: text

	MSBuild.exe csharp-generic-framework.sln /p:Configuration="Debug";Platform="All CPU"


Execution
=========
Navigate to the build directory and locate csharp-generic-framework.exe

1. To run the framework WITHOUT command-line arguments, simply double-click the executable.
2. To run the framework WITH command-line arguments, open a command prompt window and enter the following: 

.. code-block:: text

	./csharp-generic-framework.exe -h   //Will display the help menu

=======

Next Steps
**********
:doc:`./CI Integration`


