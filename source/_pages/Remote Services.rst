===============
Remote Services
===============

This page will cover creating a basic connection to some of the most common remote testing platforms.

All examples here use the RemoteWebDriver and capabilities listed in the config file. Sometimes, remote services need capabilities that are not included in the example configuration file, and they will have to be added to the framework by hand in the Config.Capabilities class. Instructions for this process are provided in the maintainence documentation.

.. todo:: link to maintainence docs above

----

Perfecto Mobile
***************

1. In your test setup method (or wherever the driver is or should be created), Create a new MutableCapabilities object assigned to the capabilities retrieved from the framework configuration.

.. code-block:: java

    // (setup method inside a test script class)

    @Before
    public void setup() throws Exception {
        // get capabilities from configuration file
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        // ...
    }

2. Below the line we just added, create a String variable. It will hold the URL that the RemoteWebDriver should use to connect.

3. Use the LoginUtils class *getLoginURL* method to construct a URL out of the given username, password, and web address. The result will need to be converted to a string, so we should also call .toString() on the result.

.. code-block:: java

    @Before
    public void setup() throws Exception {
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        // create a connection URL string
        String connUrl = LoginUtils.getLoginURL(
                "myUsername@qaconsultants.com",
                "myPassword",
                "partners.perfectomobile.com/nexperience/wd/hub")
                .toString();

        // ...
    }


**NOTE** Perfecto connections use the account's email address as the login username. Both the username and password provided to this method are the same as what you would use to login to the Perfecto dashboard website.


**NOTE** The web address shown above (third getLoginURL argument) is accurate as of writing this, but may change in the future. Check Perfecto's official documentation if any issues arise.

4. Pass both of these newly created variables to the DriverFactory *getRemoteDriver* method

.. code-block:: java

    @Before
    public void setup() throws Exception {
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        String connUrl = LoginUtils.getLoginURL(
                "myUsername@qaconsultants.com",
                "myPassword",
                "partners.perfectomobile.com/nexperience/wd/hub")
                .toString();

        // pass information to driver
        mWebDriver = DriverFactory.getRemoteDriver(caps, connUrl);
    }

----

Sauce Labs
**********

**NOTE** The Sauce Labs instructions are nearly identical to the Perfecto instructions, the main difference being the username/password (access key) and connection address in step 3.

1. In your test setup method (or wherever the driver is or should be created), Create a new MutableCapabilities object assigned to the capabilities retrieved from the framework configuration.

.. code-block:: java

    // (setup method inside a test script class)

    @Before
    public void setup() throws Exception {
        // get capabilities from configuration file
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        // ...
    }

2. Below the line we just added, create a String variable. It will hold the URL that the RemoteWebDriver should use to connect.

3. Use the LoginUtils class *getLoginURL* method to construct a URL out of the given username, password, and web address. The result will need to be converted to a string, so we should also call .toString() on the result.

.. code-block:: java

    @Before
    public void setup() throws Exception {
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        // create a connection URL string
        String connUrl = LoginUtils.getLoginURL(
                "myUsername",
                "loginAccessKey",
                "ondemand.saucelabs.com:443/wd/hub")
                .toString();

        // ...
    }

**NOTE** Sauce Labs remote connections use the account username, not an email like Perfecto does.

**NOTE** Sauce Labs does not accept an account password for their RemoteWebDriver connection process. Sauce Labs should provide you with a 32-digit access key to be used in the login URL creation code. The access key can be found under your account page.


**NOTE** The web address shown above (third getLoginURL argument) is accurate as of writing this, but may change in the future. Check Sauce Labs official documentation if any issues arise.

4. Pass both of these newly created variables to the DriverFactory *getRemoteDriver* method

.. code-block:: java

    @Before
    public void setup() throws Exception {
        MutableCapabilities caps = Config.getInstance().capabilities.getCapabilities();

        String connUrl = LoginUtils.getLoginURL(
                "myUsername",
                "loginAccessKey",
                "ondemand.saucelabs.com:443/wd/hub")
                .toString();

        // pass information to driver
        mWebDriver = DriverFactory.getRemoteDriver(caps, connUrl);
    }
    
=======

Next Steps
**********
:doc:`./Running Unit Tests`
