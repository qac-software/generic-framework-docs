==============
Creating Tests
==============

Page Object Model
*****************

What is the Page Object Model (POM)?
""""""""""""""""""""""""""""""""""""

Page Object Model (POM, though not to be confused with Maven's concept of a `POM <https://maven.apache.org/pom.html>`_) is a design pattern that makes our testing code maintainable, reusable, and easy to understand.

In a project using the Page Object Model, every web-page under test is represented by its own class with methods describing actions that can be performed on that specific web-page. These page classes all inherit from an abstract Page class, and every method returns an instance of Page (either itself, or a new instance of another implementation) which allows for operations within and between pages to be chained together.

For a more in-depth intro to the Page Object Model, check out `Selenium Easy's POM Introduction <http://www.seleniumeasy.com/selenium-tutorials/page-object-model-framework-introduction>`_.

**NOTE:** The framework comes with some example classes set up already; these are a good reference when building your own. They can be found in :code:`src/main/java/tests/examples/pages/` (C# framework: :code:`csharp-generic-framework/src/Tests/Pages/`)

----

Creating a new Page
"""""""""""""""""""

Java
----

1. Create a new Java class and give it a descriptive name. For instance: we'd create 'GoogleHomePage.java' for testing the Google home page. It is recommended to keep tests in a descriptively named package/directory such as :code:`src/main/java/tests/examples`; the examples package is where our example tests live.
 
 .. code-block:: java

    public class GoogleHomePage {
        // ...
    }

2. To leverage the common-functionality in the abstract base class, the newly created GoogleHomePage will extend from the :code:`com.qaconsultants.pom.Page` class, and pass itself as the template argument. Doing this allows for our new derived page to take advantage of method-chaining and achieve easier scripting of our pages.

 .. code-block:: java

    public class GoogleHomePage extends Page<GoogleHomePage> {
        // ...
    }

 .. code-block:: java

    //An example of method-chaining
    (new GoogleHomePage(_driver)).navigateTo("https://www.google.com")
        .click(...)
        .inputText(...)
        .close();

3. Create a constructor that accepts a :code:`org.openqa.selenium.WebDriver` and calls its super method

 .. code-block:: java

    public class GoogleHomePage extends Page<GoogleHomePage> {
        public GoogleHomePage(WebDriver _driver) {
            super(_driver)
        }
    }

4. Create new methods/functions or use the existing abstract Page functionality. 

 .. code-block:: java

    public class GoogleHomePage extends Page<GoogleHomePage> {
        public GoogleHomePage(WebDriver _driver) {
            super(_driver)
        }

        public GoogleHomePage performSearch() {
            // ...
        }
    }

Using this method in your test script then looks like the following:

 .. code-block:: java

    (new GoogleHomePage(_driver)).performSearch() 
    // ... 



C#
----

The steps for creating a test in the C# framework are much the same, but with slight syntax differences. Here is the same example from step 4, written in C#. The main differences are highlighted.

.. code-block:: C#
   :emphasize-lines: 3,5

    namespace csharp_generic_framework.POM
    {
        class GoogleHomePage : Page<LoginPage>
        {
            public GoogleHomePage(IWebDriver _driver) : base(_driver)
            {
                // ...
            }

            public GoogleHomePage performSearch()
            {
                // ...
            }
        }
    }


Interaction between Page Objects
""""""""""""""""""""""""""""""""

The Page Object Model works best when pages interact with each other. When navigating between pages, the method that performs the navigation action (whether it be clicking a link, loading a URL, etc) should return a new instance of the Page derived class that represents the web-page being navigated to. An example of this is shown below.

.. code-block:: java

    public class LoginPage extends Page<LoginPage> {
        // ... constructor, methods, etc ...

        //An example of a variable found at run-time using Selenium's PageFactory
        @FindBy(how = How.XPATH, using = "//*[@id='usersubmit']")
        private WebElement usernameSubmit;

        //An example of a variable found at run-time using methods on the WebDriver
        private WebElement passwordField = driver.findElement(By.id("PasswordID"));

        // fill in the username field and submit it
        public LoginPage setUsername(String _username) throws Exception {
            inputText(usernameField, _username);
            click(usernameSubmit); 

            // return the same instance of LoginPage (the current class)
            // because we're not navigating away from the login page
            return this;
        }

        // fill in the password field and submit it
        public LoginPage setPassword(String _password) throws Exception {
            inputText(passwordField, _username);

            // return the same instance of LoginPage (the current class)
            // because we're not navigating away from the login page
            return this;
        }

        // submit the typed password
        public LandingPage submit() throws Exception {
            waitFor(passwordSubmit);
            clickElement(passwordSubmit)

            // return a new instance of a LandingPage class, because
            // after submitting the password form, the site under test will
            // redirect us to a landing page.
            // Make sure to pass the same instance of the driver we're currently
            // using, so it can continue operating where this page left off.
            return new LandingPage(getDriver());
        }
    }

----

Scripting a Test
""""""""""""""""

Once Page classes are created, we need to write a script to actually execute the test.

1. Create a class in the :code:`src/main/java/tests/` directory (C# framework: :code:`csharp-generic-framework/src/Tests`). Class name doesn't matter, but should be descriptive.
2. Inside the class, create a private variable that is of type WebDriver.
3. Add setup and teardown methods with JUnit Before and After annotations as shown below.

.. code-block:: java

    import org.openqa.selenium.WebDriver;
    import org.junit.After;
    import org.junit.Before;
    import org.junit.Test;
    import com.qaconsultants.misc.DriverFactory;
    import tests.pages.LoginPage;

    // our test script class, in this case named TestLogin as we will be
    // testing the Login system.
    public class TestLogin {
        private WebDriver driver;

        @Before
        public void setup() throws Exception {
            // get a chrome driver from the QAC DriverFactory utility
            driver = DriverFactory.getLocalDriver(DriverFactory.Browser.CHROME);
        }

        @After
        public void teardown() {
            // make sure to clean up the driver after the test is finished;
            // close the browser and quit the driver.
            driver.close();
            driver.quit();
        }
    }

**NOTE** The following System property must be set for the DriverFactory to locate the driver executables. In the example project it is already set in the App.setSystemInformation method.

.. code-block:: java

    System.setProperty("qac.webDriverDirectory", "path/to/webdrivers");

4. Begin adding methods to call a sequence of your page class methods. These new methods should be given the JUnit Test annotation. Below we'll be calling a few methods defined in our LoginPage class (See "Interaction between page objects" section above for example implementations of these methods).

.. code-block:: java

    // ... imports ...

    public class TestLogin {

        // ... variables, setup/teardown methods, etc ...

        @Test
        public void accountLogin() throws exception {
            // create an instance of the page class our test should start on
            LoginPage _page = new LoginPage(driver);

            // start by having the driver navigate to the login page,
            // then "input username and submit", "input password and submit",
            // followed by a validation of the page title and class.
            _page.navigate()
                .setUsername("qactest365@gmail.com")
                .setPassword("qactest12")
                .submit()
                .validateTitle()
                .validatePage(LandingPage.class);

            // These actions can all be chained together since every method used
            // returns an instance of the appropriate page derived class.
        }
    }

This simple test script is now ready to be executed. Next, see the :doc:`./Run Tests` page.


C#
---

The syntax of a C# test is only slightly different. Here is the full example shown above, rewritten in C#.


.. code-block:: C#

    // .. imports ... 

    namespace csharp_generic_framework.Tests
    {
        [TestFixture]
        class TestLogin
        {
            IWebDriver driver;

            [SetUp]
            public void Setup()
            {
                // get a chrome driver from the QAC DriverFactory utility
                _driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
            }

            [TearDown]
            public void TearDown()
            {
                // make sure to clean up the driver after the test is finished;
                // close the browser and quit the driver.
                _driver.Close();
                _driver.Quit();
            }

            [Test]
            public void AccountLogin()
            {
                // create an instance of the page class our test should start on
                LoginPage _page = new LoginPage(driver);

                // start by having the driver navigate to the login page,
                // then "input username and submit", "input password and submit",
                // followed by a validation of the page title and class.
                _page.NavigateTo()
                    .SetUsername("qactest365@gmail.com")
                    .SetPassword("qactest12")
                    .Submit()
                    .ValidateTitle()
                    .ValidatePage(LandingPage.class);

                // These actions can all be chained together since every method used
                // returns an instance of the appropriate page derived class.
            }
        }
    }

=======

Next Steps
**********
:doc:`./Run Tests`
