==============
Project Layout
==============

Important Directories and Files
*******************************

**NOTE** Files and directories listed here are relative to project root.

Java
----

:code:`logs/`
    Default test logs output directory.

    Log files generated during test execution will by default be placed inside this directory, organized by test run date, test run time, and project name.

    To have log files placed into another directory, see the *Pathing > Logs* section in the :doc:`./Configuration` page.

:code:`resources/configuration/`
    Configuration file directory, holds the Config.toml file.

    See the :doc:`./Configuration` page for details on the Config.toml file.
    
:code:`resources/webdrivers/`
    Default location to store webdriver executables.
    Contains a tool to automatically download common WebDriver executables called :code:`DownloadWebDrivers.exe`. Simply run this application and the driver executables will be downloaded to the current directory, which is where the framework will look for the drivers by default.

:code:`src/main/java/com/qaconsultants/config/model/Config`
    Centralized location for config handling code. Used to control what framework configuration keys are supported by the framework. Note that this is not where configuration options are set; that is done in the *resources/configuration/* directory.

:code:`src/main/java/com/qaconsultants/keywords/*`
    Keyword implementations, categorized into directories (interaction, navigation, etc)

:code:`src/main/java/com/qaconsultants/pom/Page`
    Base class for all page object model pages.

:code:`src/main/java/tests/`
    Test files. These are classes that integrate with JUnit and call necessary test methods on Page subclasses

:code:`src/main/java/tests/pages/`
    Directory where POM Page classes should be placed.

:code:`src/main/java/tests/suites/`
    Directory for setting up suites of test scripts

:code:`src/test/java/qac/`
    Root directory for framework unit and integration tests. These are tests only for code that is internal to the framework; no client application test scripts are located in this directory (they live in :code:`src/main/java/tests/`).

:code:`src/test/java/qac/resources/Config.toml`
        Configuration for framework unit and integration tests.


C#
--

:code:`resources/logs/`
    Default test logs output directory.

    Log files generated during test execution will by default be placed inside this directory, organized by test run date, test run time, and project name.

    To have log files placed into another directory, see the *Pathing > Logs* section in the :doc:`./Configuration` page.

:code:`resources/configuration/`
    Configuration file directory, holds the Config.toml file.

    See the :doc:`./Configuration` page for details on the Config.toml file.
    
:code:`resources/webdrivers/`
    Default location to store webdriver executables.
    Contains a tool to automatically download common WebDriver executables called :code:`DownloadWebDrivers.exe`. Simply run this application and the driver executables will be downloaded to the current directory, which is where the framework will look for the drivers by default.

:code:`csharp-generic-framework/src/Configuration/`
    Centralized location for config handling code. Used to control what framework configuration keys are supported by the framework. Note that this is not where configuration options are set; that is done in the *resources/configuration/* directory.

:code:`csharp-generic-framework/src/Keywords/`
    Keyword implementations, categorized into directories (interaction, navigation, etc)

:code:`csharp-generic-framework/src/POM/`
    Base class for all page object model pages.

:code:`csharp-generic-framework/src/Tests/Pages/`
    Directory where POM Page classes should be placed.

:code:`csharp-generic-framework/src/Tests/Suites/`
    Directory for setting up suites of test scripts

:code:`csharp-generic-framework/src/UnitTests/Suites/`
    Test files. These are folders containing classes that integrate with NUnit and call necessary test methods on internal framework classes. Each directory in here represents one unit test namespace, allowing the tests to be run individually by NUnit, or as a suite by pointing the runner to the entire UnitTests namespace (See :doc:`Running Unit Tests` for more information). No client application test scripts are located in this directory (they live in :code:`csharp-generic-framework-src/Tests/Suites/[suitename]`)

Next Steps
**********
:doc:`./Create Tests`
