=====================
Working with Keywords
=====================

Keywords Overview
"""""""""""""""""

The intention behind wrapping Selenium functionality in keywords is for future integration with a keyword driven environment. For instance, a client may require tests be written using an external data format to be parsed into programmatic actions later, eg: Excel XLSX files.

Using these keyword wrappers, we can easily call the functionality that we need in a single line without having to rewrite boilerplate functionality for the new input method.

It is recommended that additional reusable Selenium functionality be implemented in Keywords for this purpose.

----

Creating a Keyword
""""""""""""""""""

1. Create a new Java/C# class with an explicit name. This name should be representative of the functionality the keyword will perform.  It is recommended to maintain packages in a single package/directory such as :code:`src/main/java/com/qaconsultants/keywords`

 .. code-block:: java 

    public SelectFromDropdown {
        ...
    }

2. The new class will extend the :code:`com.qaconsultants.keywords.Keyword` abstract class, and pass a type as a template argument. This type dictates the value accepted by the abstract Keywords Execute() method and is typically either a WebElement or WebDriver depending on the context of the action being performed. 

 .. code-block:: java 

    //Since we act on a WebElement and not the driver itself, we pass WebElement as the template type.
    public SelectFromDropdown extends Keyword<WebElement> {
        ...
    }


3. Override the Execute() method as shown below. Note the first argument corresponds to the template argument. 

 .. code-block:: java

    public SelectFromDropdown extends Keyword<WebElement> {
        @Override
        public boolean Execute(WebElement _element, String... _arguments) {
            try {
                // TODO: implement keyword behavior here
                return true
            }
            catch(Exception e) {
                // TODO: handle caught errors
                throw e;
            }
        }
    }

4. Code the Keyword implementation inside the try/catch block

----

Using Keywords
""""""""""""""

An effective use of the keyword system would be in order to drive the framework using an external file filled with keywords, such as an Excel spreadsheet.

.. todo:: link to maintainence docs below (or at least mention the correct page once that's up)

Information on using the framework in a keyword oriented way can be found in the maintainence docs on the **Keywords** page.

Keywords can be used from within page objects. Below is an example of how to call the built in "Click" keyword from within a Page derived class

*NOTE: The Page base class already utilizes built-in keywords and exposes them in methods (such as Page.clickElement). The usage below is only for example purposes.*

.. code-block:: java

    // usage inside a method:
    new Click().Execute(someWebElement);

=======

Next Steps
**********
:doc:`./Command Line`
