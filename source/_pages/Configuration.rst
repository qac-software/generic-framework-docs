=============
Configuration
=============

The framework (both the Java and C# versions) are configured using an external file written in a markup language called TOML. Read more about TOML here https://github.com/toml-lang/toml.

The framework can be configured using the .toml file located at :code:`/resources/configuration/Config.toml`.

----

Brief overview of TOML
""""""""""""""""""""""

A TOML file is broken up into sections, each with its own heading. These headings are enclosed in square brackets.

Beneath headings are key/value pairs, listed with the syntax :code:`key = "value"` The keys get turned into variables and are assigned their associated value.

An example of the capabilities section is shown below:

.. code-block:: ini

    [capabilities]
    url = "TEST URL"
    browserName = "browserName"
    browserVersion = "browserVersion"
    platform = "plat"


Most of the time, the default lines that are already in the existing Config.toml file are all we need to get started.

----

Configuration Sections
""""""""""""""""""""""
Project
*******

name
----

Project name. Used in logs.

:code:`name = "My Test Project Name"`

----

Pathing
*******

webdrivers
----------

Path to directory containing browser-specific WebDriver executables. Non-absolute paths are relative to framework root.

:code:`webdrivers = "./resources/webdrivers/"`


logs
----

Path to logs directory; all framework logs will be written to files in this directory. Non-absolute paths are relative to framework root.

:code:`logs = "./logs/"`


reports
-------

Path to reports directory; all framework reports will be written to files in this directory. Non-absolute paths are relative to framework root.

:code:`reports = "./reports/"`

----

Capabilities
************

**NOTE** For information on how to use capabilities in your project, see the page :doc:`Working with Capabilities`

url (optional)
--------------

URL for RemoteWebDriver connections.

:code:`url = "http://example.url"`

browserName (optional)
----------------------

Name of browser to use in RemoteWebDriver connections.

:code:`browserName = "mobileChrome"`

browserVersion (optional)
-------------------------

Version number of browser to use in RemoteWebDriver connections.

:code:`browserVersion = "62.0.3"`

platformName (optional)
-----------------------

Name of operating system to use in RemoteWebDriver connections.

:code:`platformName = "iOS"`


platformVersion (optional)
--------------------------

Version number of operating system to use in RemoteWebDriver connections.

:code:`platformVersion = "11.*"`


manufacturer (optional, mobile)
-------------------------------

Manufacturer of device

:code:`manufacturer = "Apple"`


model (optional, mobile)
------------------------

Model name of device.

:code:`model = "iPhone.*"`


.. todo:: MAINTANENCE DOCS LINK: link to page regarding adding support for new configuration keys etc

=======

Next Steps
**********
:doc:`./Working with Capabilities`
