===============
Getting Started
===============

Setup
*****

To get started, we have the choice of either cloning the repo as-is and modifying code on our local machines, or by forking the project on BitBucket and creating a separate repository for maintaining additions and modifications. 

It is recommended to fork the existing project so that you maintain control over your modifications.

----

Using Git Command-Line
""""""""""""""""""""""
We main method of checking out the repository from BitBucket will be through using the Git command-line tool.

1. Open a command-prompt and check whether or not Git is already installed by using the command :code:`git --help`

If the 'git' command is unrecognized, it is safe to assume that Git is not installed correctly. 
Download and install it from `here <https://git-scm.com/>`_

2. Once Git is confirmed to be installed and accessible from the command-line, use the following command:

.. code:: 

   git clone <repository url> 
   git clone https://Tremazki@bitbucket.org/qac-software/java-generic-framework.git

**NOTE**: Additional methods of cloning the repository may be provided for specific IDE's and environments in the following sections. 

----

Download WebDriver Executables
""""""""""""""""""""""""""""""
The generic framework comes packaged with an executable with the purpose of automatically downloading and extracting the WebDrivers from
their respective repositories. 

This is located in :code:`/resources/webdrivers`

**NOTE**: To download a different version of any of the drivers, the URLs in :code:`WEBDRIVER_DOWNLOAD_URLS.txt` can be updated and the downloader should still work.

----

Opening the Project
"""""""""""""""""""

Java Framework
==============

Eclipse
-------

If the project has already been cloned manually using the above Git steps.. 

1. Open Eclipse, and click on *File > Import...* and select *Maven > Existing Maven Projects* in the dialog box. Click next.
2. Browse for and select the directory of your cloned repository. Click finish. The project should be imported.
3. In the Package Explorer, find the file :code:`pom.xml`. Right click it in the Package Explorer, and select *Run As > Maven Build...*
4. In the *Goals* input field, type :code:`exec:java`. Click Run at the bottom of the window.
5. The framework should successfully run the provided example tests.

If you would prefer to checkout the project using Eclipse' built in ability to import from a Git repository..

1. Open Eclipse, and click on *File > Import...* and select *Git > Projects from Git* in the dialog box. Click next.
2. Select 'Clone URI' and click next.
3. Enter the Git location into the URI text box, the information should populate itself, and click next.
4. Select the branches you wish to pull and click next.
5. Select the destination to pull the Git repository to, set the initial branch, and click next.
6. Eclipse will now pull the repository and set up the project.
7. Import as a Maven project.
8. In the Package Explorer, find the file :code:`pom.xml`. Right click it in the Package Explorer, and select *Run As > Maven Build...*
9. In the *Goals* input field, type :code:`exec:java`. Click Run at the bottom of the window.



IntelliJ IDEA
-------------

1. Open IntellIJ, and on the welcome screen select *Import Project*. Browse for and select the directory of your cloned repository. Click OK.
2. Select the *Import project from external model* radio button, and then click Maven in the list view (these may already be selected). Click next on all the dialog boxes and finally click "Finish".
3. In the Project sidebar, locate the App class under :code:`src/main/java/com.qaconsultants/App`. Right click it in the side bar and select *Run 'App.main()'*.
4. The framework should successfully run the provided example tests.

----

C# Framework
============

1. Clone the framework git repository from either the `QAC C# framework repository <https://bitbucket.org/qac-software/csharp-generic-framework>`_ or from your own fork.
2. Open Visual Studio, and click *Open Project*, browse to the directory of your cloned repository and select :code:`csharp-generic-framework.sln`, then click *Open*.
3. Press the green arrow *Start* button at the top of the window.
4. The framework should successfully run the provided example tests.

=======

Next Steps
**********
Get familiar with the layout of the framework project: :doc:`./Project Layout`
