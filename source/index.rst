.. QAC Generic Automation Framework documentation master file, created by
   sphinx-quickstart on Tue Nov 28 09:58:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    _pages/Getting Started
    _pages/Project Layout
    _pages/Create Tests
    _pages/Run Tests
    _pages/Configuration
    _pages/Working with Capabilities
    _pages/Keyword System
    _pages/Command Line
    _pages/CI Integration
    _pages/Logging
    _pages/Reporting
    _pages/Remote Services
    _pages/Running Unit Tests

