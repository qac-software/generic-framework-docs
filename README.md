# QAC Generic Automation Framework Documentation
## Getting Started

---
## Prerequisites

- Python 2.7 or 3.x (Or any version that is compatible with Sphinx 1.5.x or higher)
- pip (should be installed by default with python 2.7.x+)

## Setup

- cd into project root
- Run `pip install sphinx` to install the Sphinx tool
- Run `pip install sphinx_rtd_theme` to install the ReadTheDocs theme used by our Sphinx configuration


## Generate Docs

##### Windows

Run `.\\docs\\make.bat html` in the project root.

##### Linux / macOS

Run `make html` in the project root.

Once built, the documentation will be in the build/html directory.

##### Hot Reloading

Install **sphinx-autobuild** using pip:

`pip install sphinx-autobuild`

Run `make auto` or `sphinx-autobuild source build/html` in the project root

Console output will display a URL, usually `http://127.0.0.1:8000`. Open this URL in your browser, and make your changes to .rst files. The browser should auto-reload and display your changes after a short delay.

## Edit Docs

Pages are written as reStructuredText (.rst) files under the `source/_pages/` directory.

Links to pages are added to the navigation sidebar by editing the **toctree** list inside `source/index.rst`. Simply add the page's relative file path to the list, minus its .rst file extension.

```
. toctree::
   :maxdepth: 2
   :caption: Contents:

   _pages/MyFirstPage
   _pages/Second Page
```
